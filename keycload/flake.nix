{
  description = "Java dev env";

  inputs.nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";

  outputs = { self, nixpkgs, ... }@inputs:
    let
      system = "x86_64-linux";
      pkgs = nixpkgs.legacyPackages.${system};
    in
    {
      devShells.x86_64-linux.default = pkgs.mkShell {
        name = "Java";
        buildInputs = with pkgs; [
          jdk21
          maven
        ];
        shellHook = "exec zsh";
      };
    };
}
