#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

void *threadFun(void *num) {
  int err, val, valAttr;
  pthread_attr_t attributs;

  val = *((int *)num);
  err = pthread_attr_init(&attributs);
  if (err != 0) {
    perror("Échec pthread_attr_init");
  };
  printf("Nouveau thread, nb : %d\n", val);

  err = pthread_attr_getdetachstate(&attributs, &valAttr);
  if (err != 0) {
    perror("Échec pthread_attr_getdetachstate");
  };
  printf("Detach state : %d\n", valAttr);

  err = pthread_attr_getschedpolicy(&attributs, &valAttr);
  if (err != 0) {
    perror("Échec pthread_attr_getschedpolicy");
  };
  printf("Sched policy : %d\n", valAttr);

  err = pthread_attr_getinheritsched(&attributs, &valAttr);
  if (err != 0) {
    perror("Échec pthread_attr_getinheritsched");
  };
  printf("Inherited sched : %d\n", valAttr);

  err = pthread_attr_getscope(&attributs, &valAttr);
  if (err != 0) {
    perror("Échec pthread_attr_getscope");
  };
  printf("Scope : %d\n", valAttr);
}

int main() {
  /* Déclaration des variables */
  int err, nb;
  pthread_t idThread;
  nb = 42;

  err = pthread_create(&idThread, NULL, threadFun, (void *)&nb);
  if (err != 0) {
    /* La création du thread fils a échoué */
    perror("Échec pthread_create\n");
  } else {
    printf("Succès pthread_create\n");
  }

  sleep(1);
}
