#include <ctype.h>
#include <math.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int add(int a, int b) { return a + b; }

int sub(int a, int b) { return a - b; }

int mul(int a, int b) { return a * b; }

typedef struct {
  int (*fn)(int, int);
  int op1;
  int op2;
} ope;

typedef struct {
  ope ope;
  int *res;
} res_ope;

void *t1(void *in) {
  res_ope *ptr = (res_ope *)in;

  *ptr->res = ptr->ope.fn(ptr->ope.op1, ptr->ope.op2);
}

void *t2(void *in) {
  static int res;

  ope *ptr = (ope *)in;
  res = ptr->fn(ptr->op1, ptr->op2);

  return &res;
}

int main() {
  /* Déclaration des variables */
  ope ope1;
  res_ope ope2;
  int *res_t1, *res_t2;
  int wait1, wait2;
  pthread_t idT1, idT2;

  /* Récupération de l’entrée utilisateur */
  printf("Premier opérande : \n");
  scanf("%d", &ope1.op1);

  {
    char operator;
    printf("Opération (+, -, *) : \n");
    scanf("%c", &operator);
    switch (operator) {
    case '+':
      ope1.fn = add;
      break;
    case '-':
      ope1.fn = sub;
      break;
    case '*':
      ope1.fn = mul;
      break;
    }
  }

  printf("Second opérande : \n");
  scanf("%d", &ope1.op2);

  /* Execution des threads */
  if (pthread_create(&idT1, NULL, t1, (void *)&ope1) != 0) {
    perror("Échec pthread_create 1\n");
  }

  ope2.ope = ope1;

  if (pthread_create(&idT2, NULL, t1, (void *)&ope2) != 0) {
    perror("Échec pthread_create 2\n");
  }

  while (wait1 != 0 && wait2 != 0) {
    wait1 = pthread_join(idT1, (void **)&res_t1);
    if (wait1 == 0) {
      printf("\n");
      printf("Thread 1 of id: %ld result : %d\n", t1, res_t1);
    }
    wait2 = pthread_join(idT2, NULL);
    if (wait2 == 0) {
      printf("Thread 2 of id: %ld result : %d\n", t2, res_t2);
    }
  }

  exit(0);
}
