#include <ctype.h>
#include <math.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

void *thread1(void *input) {
  static long rounded;
  double num;
  pthread_t id;

  printf("I’m the first thread\n");

  id = pthread_self();
  if (id == -1) {
    perror("Fail pthread_self");
  };
  printf("TID first thread : %ld\n", id);

  num = *((double *)input);
  printf("Params first thread (nb) : %f\n\n", num);

  rounded = round(num);

  pthread_exit(&rounded);
}

void(*thread2(void *input)) {
  static char res[50];
  char *str = (char *)input;
  pthread_t id;

  printf("Je suis le second thread\n");

  id = pthread_self();
  if (id == -1) {
    perror("Échec pthread_self");
  };
  printf("TID second thread : %ld\n", id);

  printf("Paramètres second thread (str) : %s\n\n", str);

  for (int i = 0; str[i] != '\0'; i++) {
    res[i] = toupper(str[i]);
  }

  pthread_exit(res);
}

int main() {
  /* Déclaration des variables */
  int wait1 = 1, wait2 = 1;
  long res1;
  char *res2;
  pthread_t idT1, idT2;
  double nb = 42.5;
  char str[] = "La chaine de characteres";

  if (pthread_create(&idT1, NULL, thread1, (void *)&nb) != 0) {
    perror("Échec pthread_create 1\n");
  }

  if (pthread_create(&idT2, NULL, thread2, (void *)&str) != 0) {
    perror("Échec pthread_create 2\n");
  }

  while (wait1 != 0 && wait2 != 0) {
    wait1 = pthread_join(idT1, (void **)&res1);
    if (wait1 == 0) {
      printf("\n");
      printf("Thread %ld result : %ld\n", idT1, *(long *)res1);
    }
    wait2 = pthread_join(idT2, (void **)&res2);
    if (wait2 == 0) {
      printf("Thread %ld result : %s\n", idT2, res2);
    }
  }

  // sleep(1);
  exit(0);
}
