#include <stdio.h>
#include <stdlib.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <unistd.h>

#define CHILDREN 5
#define CPT_ADD 10000

int main() {
  int shm;
  int *cpt;

  // Allocation et attachement de la mémoire partagée
  shm = shmget(IPC_PRIVATE, sizeof(int),
               IPC_CREAT | IPC_EXCL | S_IRUSR | S_IWUSR);
  cpt = (int *)shmat(shm, NULL, 0);
  *cpt = 0;

  printf("Lancement des %d processus fils\n", CHILDREN);
  // Lancement de tous les fils
  for (int i = 0; i < CHILDREN; i++) {
    if (fork() == 0) {
      for (int i = 0; i < CPT_ADD; i++) {
        (*cpt)++;
      }
      printf("Processus fils %d : Compteur = %d\n", getpid(), *cpt);
      exit(0);
    }
  }

  // Attente de tous les fils
  for (int i = 0; i < CHILDREN; i++) {
    wait(NULL);
  }

  // Affiche la valeur du compteur
  printf("Processus parent : Compteur = %d\n", *cpt);

  // Détache et supprime la mémoire partagée
  shmdt(cpt);
  shmctl(shm, IPC_RMID, 0);
  exit(0);
}
