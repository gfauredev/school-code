{
  description = "C dev env";

  inputs.nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";

  outputs = { self, nixpkgs, ... }@inputs:
    let
      system = "x86_64-linux";
      pkgs = nixpkgs.legacyPackages.${system};
      # stdenv = pkgs.clangStdenv;
      stdenv = pkgs.stdenv;
    in
    {
      devShells.x86_64-linux.default = stdenv.mkDerivation {
        name = "C";
        buildInputs = with pkgs; [
          clang
        ];
        shellHook = "exec zsh";
      };
    };
}
