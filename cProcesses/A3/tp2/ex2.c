#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

void *tFn(void *num) {
  int order = *(int *)num;
  char phrase[] = "Je suis le thread N";
  for (int i = 0; i < sizeof(phrase) / sizeof(phrase[0]); i++) {
    printf("%c", phrase[i]);
    usleep(100);
  }
  printf("%d\n", order);
}

int main() {
  int err;
  int order[] = {1, 2, 3, 4, 5, 6};
  pthread_t idTs[2];

  for (int i = 0; i < sizeof(idTs) / sizeof(idTs[0]); i++) {
    err = pthread_create(&idTs[i], NULL, tFn, (void *)&order[i]);
    usleep(10000);
    if (err != 0) {
      perror("Échec pthread_create\n");
    }
  }
}
