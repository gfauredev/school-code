#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int cpt_global = 0;

void *tFn_global() {
  for (int i = 0; i < 10000; i++) {
    cpt_global++;
  }
}

void *tFn_local(void *num) {
  int *ptr = num;
  for (int i = 0; i < 10000; i++) {
    *ptr = *ptr + 1;
  }
}

int main() {
  int err;
  int cpt_local = 0;
  pthread_t idTs[5];

  for (int i = 0; i < sizeof(idTs) / sizeof(idTs[0]); i++) {
    err = pthread_create(&idTs[i], NULL, tFn_global, NULL);
    usleep(1);
    if (err != 0) {
      perror("Échec pthread_create\n");
    } else {
      printf("Succès pthread_create(%p, NULL, %p, NULL)\n", &idTs[i],
             tFn_global);
    }
  }

  printf("\n\tglobal cpt = %d\n\n", cpt_global);

  for (int i = 0; i < sizeof(idTs) / sizeof(idTs[0]); i++) {
    err = pthread_create(&idTs[i], NULL, tFn_local, (void *)&cpt_local);
    usleep(1);
    if (err != 0) {
      perror("Échec pthread_create\n");
    } else {
      printf("Succès pthread_create(%p, NULL, %p, %p)\n", &idTs[i], tFn_local,
             (void *)&cpt_local);
    }
  }

  printf("\n\tlocal cpt = %d\n\n", cpt_local);
}
