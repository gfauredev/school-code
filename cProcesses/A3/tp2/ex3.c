#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define PHILOS 3
#define MAX_MEAL 5
#define WAIT_NS 1024

pthread_mutex_t forks[PHILOS] = PTHREAD_MUTEX_INITIALIZER;

void *philo(void *in) {
  int id = *((int *)in);
  printf("Éveil philosophe %d\n", id);
  int meals = 0;

  pthread_mutex_t l_fork = forks[id];
  pthread_mutex_t r_fork = forks[(id + 1) % PHILOS];

  while (meals < MAX_MEAL) {
    printf("Philosophe %d prend la fourchette gauche (%d)\n", id, id);
    if (pthread_mutex_lock(&l_fork) == 0) {
      printf("Philosophe %d essaye de prendre la fourchette droite (%d)\n", id,
             (id + 1) % PHILOS);
      if (pthread_mutex_trylock(&r_fork) == 0) {
        meals++;
        printf("Philosophe %d mange son repas n%d\n", id, meals);
        printf("Philosophe %d rend les fourchettes\n", id);
        pthread_mutex_unlock(&l_fork);
        pthread_mutex_unlock(&r_fork);
      } else {
        printf("Philosophe %d ne peut pas prendre la fourchette droite, "
               "rend la fourchette gauche\n",
               id);
        pthread_mutex_unlock(&l_fork);

        struct timespec *times;
        times->tv_nsec = random() % (long)WAIT_NS;
        printf("Philosophe %d commence à réfléchir pour %ld ns\n", id,
               times->tv_nsec);
        nanosleep(times, NULL); // Réfléchir un temps aléatoire
        printf("Philosophe %d arrête de réfléchir\n", id);
      }
    } else {
      perror("Échec mutex lock\n");
    }
  }

  int res = 0;
  pthread_exit(&res);
}

int main() {
  int count[PHILOS];
  long res;
  pthread_t idTs[PHILOS]; // Déclaration des ID des threads

  // Lancement tous les threads
  for (int i = 0; i < PHILOS; i++) {
    // printf("Lancement philosophe %d\n", i);
    count[i] = i;
    if (pthread_create(&idTs[i], NULL, philo, &count[i]) != 0) {
      perror("Échec pthread create\n");
    }
  }

  // Mise en attente et affichage des résultats
  for (int i = 0; i < PHILOS; i++) {
    // printf("Jointure philosophe %d\n", i);
    if (pthread_join(idTs[i], (void **)&res) == 0) {
      if (*(int *)res == 0) {
        printf("Philosophe %d est rasasié !\n", i);
      } else {
        printf("Philosophe %d retourne %d\n", i, *(int *)res);
      }
    } else {
      perror("Échec pthread join\n");
    }
  }

  return 0;
}
