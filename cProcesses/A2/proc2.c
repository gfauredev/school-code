#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>
#include <wait.h>

int main() {

  // Déclaration des variables
  pid_t idProc;
  int report, signal, status;
  time_t t = time(NULL);

  char *args[] = {"p2fils2", NULL};

  // présentation processus père
  printf("At the date %s my UID is %d\n", ctime(&t), getuid());

  // creation fils1
  idProc = fork();
  switch (idProc) {
  case -1:
    perror("failed fork f1");
    exit(1);
  case 0:
    if (execl("./p2fils1", "p2fils1", NULL) == -1) {
      perror("echec execl");
      exit(2);
    }
    exit(1);
  }

  // creation fils2
  idProc = fork();
  switch (idProc) {
  case -1:
    perror("failed fork f2");
    exit(1);
  case 0:
    if (execvp("./p2fils2", args) == -1) {
      perror("echec execl");
      exit(2);
    }
    exit(1);
  }

  // attente de la terminaison de tous les fils par le père
  idProc = wait(&report);
  while (idProc != -1) {
    signal = report & 0x7F;
    switch (signal) {
    case 0:
      status = (report >> 8) & 0xFF;
      printf("process %d terminated with status %d\n", idProc, status);
      break;

    default:
      printf("process %d dead with signal number %d\n", idProc, signal);
      break;
    }
    idProc = wait(&report);
  }

  return 0;
}
