#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

/* fonction de traitement du fils1 */
/* affiche son pid et se termine normalement en retournant 3 */
int main() {
  printf("I am fils1, my PID is %d\n", getpid());
  exit(3);
}
