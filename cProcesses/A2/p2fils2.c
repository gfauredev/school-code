#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

/* fonction de traitement du fils2 */
/* affiche son pid et tente d'écrire en mémoire à l'adresse NULL */
int main() {
  printf("I am fils2, my PID is %d\n", getpid());
  //
  //
  // Pour écrire à une adresse mémoire interdite littéralement :
  // L'entier qui se trouve à l'adresse NULL reçoit la valeur 45 (par exemple)
  // On n'a pas le droit d'écrire ou de lire à cette adresse
  *(int *)NULL = 45;
}
