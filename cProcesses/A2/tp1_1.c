#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>
#include <wait.h>

/* traitement des fils */
void fils1();
void fils2();
void fils3();

int main() {

  /* Déclaration des variables */
  pid_t idProc;
  int report, numSig, status;
  time_t t = time(NULL);

  /* présentation processus père */
  printf("At the date %s, my UID is %d\n", ctime(&t), getuid());

  /* creation fils1 */
  idProc = fork();
  switch (idProc) {
  case -1:
    perror("failed fork f1");
    exit(1);
  case 0:
    fils1();
    exit(1);
  }

  /* creation fils2 */
  idProc = fork();
  switch (idProc) {
  case -1:
    perror("failed fork f2");
    exit(1);
  case 0:
    fils2();
    exit(1);
  }

  /* creation fils3 */
  // f3 = fork() ;
  idProc = fork();
  // switch  (f3)
  switch (idProc) {
  case -1:
    perror("failed fork f3");
    exit(1);
  case 0:
    fils3();
    exit(1);
  }

  /* suite du processus pere */
  /* attente de la terminaison de tous les fils */

  // Boucle wait() d'attente de terminaison de tous les fils
  idProc = wait(&report);
  while (idProc != -1) {
    numSig = report & 0x7F;
    switch (numSig) {
    case 0:
      status = (report >> 8) & 0xFF;
      printf("process %d terminated with status %d\n", idProc, status);
      break;

    default:
      printf("process %d dead with signal number %d\n", idProc, numSig);
      break;
    }
    idProc = wait(&report);
  }

  return 0;
}

/* fin du processus pere */

/* fonction de traitement du fils1 */
/* affiche son pid et se termine normalement en retournant 3 */
void fils1() {
  printf("I am fils1, my PID is %d\n", getpid());
  exit(3);
}

/* fonction de traitement du fils2 */
/* affiche son pid et tente d'écrire en mémoire à l'adresse NULL */
void fils2() {
  printf("I am fils2, my PID is %d\n", getpid());
  //
  //
  // Pour écrire à une adresse mémoire interdite littéralement :
  // L'entier qui se trouve à l'adresse NULL reçoit la valeur 45 (par exemple)
  // On n'a pas le droit d'écrire ou de lire à cette adresse
  *(int *)NULL = 45;
}

/* fonction de traitement du fils3 */
/* affiche son pid et tente d'effectuer une division sournoise par zéro */
void fils3() {
  printf("I am fils3, my PID is %d\n", getpid());
  int i, v = 10;
  i = 10;
  v = 1 / (v - i);
}
