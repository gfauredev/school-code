#! /usr/bin/env nix-shell
#! nix-shell -i python3 -p python310Packages.numpy

import numpy as np

lam = 2171669;

def duree(l=lam, n=4, m=10000):
    return np.random.exponential(l, (n, m))

def serie(l=lam, n=4, m=10000):
    return round(duree(l, n, m).min(axis=1).mean())

def parallel(l=lam,n=4,m=10000):
    return round(duree(l, n, m).max(axis=1).mean())

def redondance(l=lam):#,n=3,m=10000,r=2):
    return round(duree(l).sorted(1).mean())

print("\nDurées de vie expérimentales")
print(duree())

print("\nDurée de vie moyenne du système en série")
print(serie())

print("\nDurée de vie moyenne du système en parallèle")
print(parallel())

print("\nDurée de vie moyenne du système avec r=2")
# print(redondance())

print("\nMTBF système exercice 2")
mtbf=duree(1, 1, 1000) + duree(1, 1, 1000))
