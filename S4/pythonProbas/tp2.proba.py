#!/run/current-system/sw/bin/python3

import numpy as np 
import matplotlib.pyplot as plt
import math

num_bins = 50

loi = "gaussienne"
mu = 100
sigma = 15
x = mu + sigma * np.random.randn(1000)
# plt.hist(x, num_bins, density=True)

loi = "normale"
absc=np.arange(x.min(), x.max(), 0.1)
# plt.plot(absc, ( 1 / (math.sqrt(2*math.pi)*sigma) ) * math.e ** (- (absc - mu)**2 / (2 * sigma)**2))

loi = "suite pseudo-aléatoire"
termes = 10000
a = 15729
b = 5625
m = 2**31
x = [3]

for i in range(termes):
    x.append((a * x[-1] + b) % m)

x = np.array(x)/m

plt.hist(x, density=True)
plt.plot([0,1], [1,1], color="red", label="Loi uniforme")

plt.title('Histogramme d’une ' + loi)
plt.xlabel('Valeur')
plt.ylabel('Probabilité')

plt.show()
