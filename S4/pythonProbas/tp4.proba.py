#!/run/current-system/sw/bin/python3

import numpy as np 
# import os
# os.environ['QT_QPA_PLATFORM_PLUGIN_PATH'] = '/chemin/vers/plugins'
import matplotlib.pyplot as plt
import math

M = [0]

def marche(M, n=10000, p=0.5):
    for u in np.random.rand(n-1):
        if u < p:
            M.append(M[-1]+1)
        else:
            M.append(M[-1]-1)

# Nombre de pas pour chaque simulation
n_steps = 1000

# Valeur de p pour les deux simulations
p1 = 0.7
p2 = 0.49

# Génération des séries de nombres aléatoires pour les deux simulations
series1 = np.random.binomial(1, p1, size=n_steps)
series2 = np.random.binomial(1, p2, size=n_steps)

# Calcul des maximums et des minimums pour chaque simulation
max1 = max(series1)
max2 = max(series2)
min1 = min(series1)
min2 = min(series2)

# Génération des histogrammes pour les maximums et les minimums
plt.hist([max1, max2], label=['max1', 'max2'])
plt.legend()
plt.show()

plt.hist([min1, min2], label=['min1', 'min2'])
plt.legend()
plt.show()
