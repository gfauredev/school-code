#! /usr/bin/env nix-shell
#! nix-shell -i python3 -p python310Packages.numpy

######################
# Imports de modules #
######################

import numpy as np
import warnings
warnings.filterwarnings('error')

######################
# Fonctions à écrire #
######################

def gauss_jordan(A,B):
    nbRows = A.shape[0]

    if nbRows != B.shape[0]:
        raise ValueError("Les matrices n’ont pas le même nombre de lignes")

    for i in range(nbRows):
        if A[i,i] != 0:
            A[i,:] /= A[i,i]
        else:
            for k in range(i, nbRows):
                if A[k,i] != 0:
                    A[i,:] += A[k,:]
                    break

        if A[i,i] == 0:
            continue

        for j in range(nbRows):
            if j != i:
                A[j,:] += A[i,:] * (-A[j,i]/A[i,i])

    return A


#####################
# Fonctions de test #
#####################

#exerice 1
def test_error():
    print("######################")
    print("# Test de contrainte #")
    print("######################\n")
    try :
        gauss_jordan(np.eye(5,5),np.ones((4,1)))
    except ValueError:
        try :
            gauss_jordan(np.eye(5,5),np.ones((5,1)))
        except ValueError:
            print("Echec : Erreur levée qui n'aurait pas du l'être !\n")
        else:
            print("Succès\n")
    else:
        print("Echec : Erreur non levée !\n")
    return


#exercice 2
def test_diago_inv1():
    print("#############################")
    print("# Test de diagonalisation 1 #")
    print("#############################\n")
    
    P = np.array([[3,2,5,1,2],
                  [2,0,5,1,3],
                  [0,4,2,1,4],
                  [3,5,1,2,0],
                  [2,2,0,1,3]
                 ],dtype=float)
    
    result = gauss_jordan(P.copy(),np.zeros((5,1)))
    
    if type(result) == tuple :
        result = result[0]
        
    if (result == np.eye(5)).all():
        print("Succès\n")
    else:
        print("Echec : le résultat n'est pas la matrice identité mais :\n")
        print(result)
        print("\n")
    return


#exercice 3
def test_diago_inv2():
    print("#############################")
    print("# Test de diagonalisation 2 #")
    print("#############################\n")
    
    Q = np.array([[3,0,5,1,2],
                  [2,0,5,1,3],
                  [0,4,2,1,4],
                  [3,5,1,2,0],
                  [2,2,0,1,3]
                 ],dtype=float) 
    try :
        result = gauss_jordan(Q.copy(),np.zeros((5,1)))
    
        if type(result) == tuple :
            result = result[0]
    
        if (result == np.eye(5)).all():
            print("Succès\n")
        else:
            print("Echec : le résultat n'est pas la matrice identité mais :\n")
            print(result)
            print("\n")
    except RuntimeWarning:
        print("Cas avec le 0 sur la diagonale non implémenté.\n")
    return


#exercice 3
def test_diago_ninv():
    print("#############################")
    print("# Test de diagonalisation 3 #")
    print("#############################\n")
    
    R = np.array([[3,2,5,1,2],
              [2,0,5,1,3],
              [0,4,2,1,4],
              [3,5,1,2,0],
              [3,5,1,2,0]
             ],dtype=float)  
    
    try:
        result = gauss_jordan(R.copy(),np.zeros((5,1)))
    
        solution = np.array([[ 1.,0.,0.,  0., -1.471],
                             [ 0.,  1.,  0.,  0.,  0.235],
                             [ 0.,  0.,  1.,  0.,  0.961],
                             [ 0.,  0.,  0.,  1.,  1.137],
                             [ 0.,  0.,  0.,  0.,  0.]])
    
        if type(result) == tuple :
            result = result[0]
    
        try:    
            if (abs(result - solution) < 0.001).all():
                print("Succès\n")
            else:
                print("Echec : le résultat n'est pas correct !\n")
        except TypeError:
            print("La fonction ne renvoie rien !\n")
    except RuntimeWarning:
        print("Cas avec le 0 sur la diagonale non implémenté.\n")
    return


#exercice 4
def test_second_membre():
    print("###########################")
    print("# Test calcul du résultat #")
    print("###########################\n")
    
    P = np.array([[3,2,5,1,2],
                  [2,0,5,1,3],
                  [0,4,2,1,4],
                  [3,5,1,2,0],
                  [2,2,0,1,3]
                 ],dtype=float)
    
    solution = np.array([0.412, 0.294, 0.118, -1.412, 0])
    
    try:
        if (gauss_jordan(P.copy(),np.array([1.,0,0,0,0]))[1] - solution < 0.001).all():
            print("Succès\n")
        else:
            print("Echec : le résultat n'est pas correct !\n")
    except TypeError:
        print("La fonction ne renvoie rien !\n")
    return


#exxercice 5
def test_exact():
    print("##########################")
    print("# Test résolution exacte #")
    print("##########################\n")
    
    try:
        S = np.array([[-0.9,0.1,0.2,0.2,0.3],
                      [0.3,-1.0,0.1,0.2,0.1],
                      [0.0,0.1,-0.7,0.1,0.1],
                      [0.1,0.2,0.1,-0.7,0.0],
                      [0.5,0.6,0.3,0.2,-0.5]
                     ])
    
        result = gauss_jordan_exact(S.copy(),np.array([0,0,0,0,0]))[0]
        
        solution = np.array([[1, 0, 0, 0, -33/73],
                             [0, 1, 0, 0, -64/219],
                             [0, 0, 1, 0, -46/219],
                             [0, 0, 0, 1, -13/73],
                             [0, 0, 0, 0, 0]
                            ])
    
        if (abs(result - solution) < 0.001).all():
            print("Succès\n")
        elif (result == np.eye(5)).all():
            print("Echec : le résultat exact ne doit pas être la matrice identité !\n")
        else:
            print("Echec : le résultat n'est pas correct !\n")
    except NameError:
        print("Fonction non implémentée !")
    return

#######################
# Programme principal #
#######################

if __name__ == "__main__":
    test_error()
    test_diago_inv1()
    test_diago_inv2()
    test_diago_ninv()
    test_second_membre()
    test_exact()
