#!/run/current-system/sw/bin/python3
#!/bin/python3

import numpy as np
import matplotlib.pyplot as plt
# import math

# Exercice 3 TP noté

# Rs = (1 − (R1 × R2) × (1 − R3)) × R4

def duree(l=.2,n=10000):
    return np.random.exponential(l,n)

durees = np.array([duree(), duree(), duree(), duree()])

def dureesSystem(d):
    rep = []
    for i in range(d[0].shape[0]):
        rep.append(min(max(min(d[0][i],d[1][i]),d[2][i]),d[3][i]))
    return np.array(rep)

print(dureesSystem(durees).mean())

plt.hist(dureesSystem(durees))
