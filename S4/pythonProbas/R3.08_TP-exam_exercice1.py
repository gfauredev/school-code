#!/run/current-system/sw/bin/python3
#!/bin/python3

import numpy as np
# import matplotlib.pyplot as plt
# import math

# Exercice 1 TP noté

def CM_5(P, mu, n=5):
    rep = mu
    for i in range(n):
        rep = np.dot(rep, P)
    return rep

M = np.array([[.3, .3, .4],
             [.2, .7, .1],
             [.5, .5, 0]])

mu0 = np.array([.2, .2, .6])

print(CM_5(M, mu0))
