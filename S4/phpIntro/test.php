<!DOCTYPE HTML>
  <html>
    <head>
      <title>Page de test</title>
    </head>
    <body>
      <h1>Page de test </h1>
        <p>
        <?php
          echo "Hello World !<br/>Bonjour à tous !";
        ?>
        </p>
        <p>
        <?php
          $age=19;
          echo "J’ai ".$age." ans !";
        ?>
        <p>
        <?php
          echo "J’ai ";
          echo $age;
          echo " ans !<br/>";
        ?>
        </p>
      <h1>Chaînes</h1>
        <p>
        <?php
          $chaine1="Une chaîne de caractère !"; $chaine2="Une chaîne de caractère !<br>";
          echo "chaine1=".$chaine1." | chaine2=".$chaine2;
          if ($chaine1 == $chaine2) {
            echo "\$chaine1 et \$chaine2 sont égaux<br>";
          }
          $rep=strcmp($chaine1, $chaine2);
          echo "$rep";
          if ($rep == 0) {
            echo "\$chaine1 et \$chaine2 sont égaux<br>";
          }
        ?>
        </p>
    </body>
</html>
