<?php
// Connexion à MySQL
  try {
    $db = new PDO("mysql:host=localhost;dbname=test", "tester", "password");
  } catch (Exception $e) {
    die('Erreur : ' . $e->getMessage());
    echo "<h1>Impossible de se connecter à la db !</h1>";
  }

// Creation table adresses
  $db->exec('create table if not exists adresses(
  lastn  varchar(50) not null,
  firstn varchar(50),
  addr   varchar(50),
  postc  char(5) not null, 
  city   varchar(50) not null,
  tel    char(10) not null
  )');

// Assignation des champs du formulaire à des variables
  $lastn  = $_POST['lastn'];
  $firstn = $_POST['firstn'];
  $addr   = $_POST['addr'];
  $postc  = $_POST['postc'];
  $city   = $_POST['city'];
  $tel    = $_POST['tel'];

// Preparation de la requete SQL d'insertion
  $insert = $db->prepare('
insert into adresses(lastn, firstn, addr, postc, city, tel)
values (:lastn, :firstn, :addr, :postc, :city, :tel)');
  
  if ($insert == false) die("Probleme prepare");

// Execution de la requete SQL d'insertion des donnees
  $insert->execute(array('lastn'  => $lastn,
                     'firstn' => $firstn,
                     'addr'   => $addr,
                     'postc'  => $postc,
                     'city'   => $city,
                     'tel'    => $tel
  ));
?>
