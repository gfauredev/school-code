#!python
from sklearn.datasets import make_blobs
from sklearn.svm import LinearSVC

# On fabrique un jeu de données aléatoires
X, y = make_blobs(n_samples=40, centers=2, random_state=0)
clf = LinearSVC(C=100).fit(X, y)

import matplotlib.pyplot as plt
from sklearn.inspection import DecisionBoundaryDisplay

fig, ax = plt.subplots()
ax.scatter(X[:, 0], X[:, 1], c=y, s=30, cmap=plt.cm.Paired)
DecisionBoundaryDisplay.from_estimator(clf, X, ax=ax, plot_method="contour", levels=[-1,0,1], linestyles=["--","-","--"])

plt.show()
