#!python
from sklearn.datasets import make_blobs
from sklearn.svm import LinearSVC

# On fabrique un jeu de données aléatoires
X, y = make_blobs(n_samples=40, centers=2, random_state=0)
clf = LinearSVC(C=100).fit(X, y)

import matplotlib.pyplot as plt
from sklearn.inspection import DecisionBoundaryDisplay

fig, ax = plt.subplots()
ax.scatter(X[:, 0], X[:, 1], c=y, s=30, cmap=plt.cm.Paired)
DecisionBoundaryDisplay.from_estimator(clf, X, ax=ax, plot_method="contour", levels=[-1,0,1], linestyles=["--","-","--"])

import numpy as np

Xnew = np.array([[0,0],[0,3],[2,2],[2,3]])
ynew = clf.predict(Xnew)
ax.scatter(Xnew[:,0], Xnew[:,1], c=ynew, s=100, cmap=plt.cm.Paired)

plt.show()
