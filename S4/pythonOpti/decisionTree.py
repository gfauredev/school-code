#!python
import pandas
from sklearn import preprocessing
from sklearn.tree import DecisionTreeClassifier, export_graphviz
from six import StringIO
from IPython.display import Image
import pydot

str_to_int = preprocessing.LabelEncoder();

# Dataframe read from file 
df = pandas.read_csv("jouerTennis.csv");
df = df.apply(str_to_int.fit_transform)

# print(df.head())
print(df.describe())

feature_cols = ['temps', 'humidite', 'temperature', 'vent']

x=df[feature_cols]
y=df.jouer

classifier = DecisionTreeClassifier(criterion="entropy")
classifier.fit(x,y)

dot_data = StringIO()
export_graphviz(classifier, out_file=dot_data, filled=True, rounded=True, special_characters=True, feature_names=feature_cols, class_names=['0','1'])
graph = pydot.graph_from_dot_data(dot_data.getvalue())
graph.write_png('Play_Tennis.png')
Image(graph.create_png())
