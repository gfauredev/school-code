package templatemethod;

public class App {
    public String getGreeting() {
        return "Goodbye World!";
    }

    public static void main(String[] args) {
        System.out.println(new App().getGreeting());
    }
}
