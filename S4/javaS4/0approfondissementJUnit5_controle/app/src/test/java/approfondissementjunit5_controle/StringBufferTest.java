import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class StringBufferTest {
  @Test
  // @DisplayName("Test charAt returned position")
  public void testCharAt(){
    MyString testStr = new MyString("interesting content");
    assertEquals(testStr.charAt(1), 'n');
  }
}
