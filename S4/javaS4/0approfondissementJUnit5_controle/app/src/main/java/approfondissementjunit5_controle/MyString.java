public class MyString {
	
    private StringBuffer sb;

    public MyString(String s) {
        this.sb = new StringBuffer(s);
    }

    public char charAt(int i) {
      return 'a';
    }

    public String getString() { 
    	return this.sb.toString();
    }
    
    public void setString(String s) { 
    	this.sb = new StringBuffer(s);
    }

}
