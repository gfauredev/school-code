
public class TriParTable implements Iterable<Integer> {

	private int[] itemsTriés;
	
	protected TriParTable(int nombreElémentsATrier) {
		this.itemsTriés = new int[nombreElémentsATrier];
	}

	private boolean itemTrouvé(int valeur) {
		return this.itemsTriés[valeur] > 0;
	}
	
	protected final int nbOccurences(int valeur) {
		return this.itemsTriés[valeur];
	}

	protected final void addItem(int valeur) {
		this.itemsTriés[valeur]+=1;
	}
	
	public IteratorTableau createIterator() {
		return new MyIterateurTableau();
	}
	
	private class MyIterateurTableau implements Iterator<Integer> {
		
		private int curseur = -1;

		public boolean hasNext() {
			for (int i = curseur + 1; i < itemsTriés.length; i++) {
				if (itemTrouvé(i))
					return true;
			}
			return false;
		}

		public int next() {
			for (int i = curseur + 1; i < itemsTriés.length; i++) {
				if (itemTrouvé(i)) {
					curseur = i;
					return curseur;
				}
			}
			return -1;
		}		
	}
}
