
public interface IteratorTableau {
	
	public boolean hasNext();
	public int next();

}
