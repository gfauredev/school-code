
public class Loterie extends TriParTable {
	
	public Loterie() {
		super(60);
	}
	
	public String boulesSorties() {
		IteratorTableau it = this.createIterator();
		String res = "";
		while (it.hasNext()) {
			int i = it.next();
			if (res.equals(""))
				res += i;
			else
				res = res + " " + i;
		}
		return res;
	}

	public void addBoule(int valeur) {
		this.addItem(valeur);
	}
}
