import static org.junit.jupiter.api.Assertions.*;

import java.util.IllegalFormatException;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class AssertThrowsTest {

	@Test
	@DisplayName("La m�thode parseInt doit lever une NumberFormatException")
	void testExpectedException() {
		NumberFormatException thrown = assertThrows(NumberFormatException.class, () -> {
			Integer.parseInt("One");
		}, "NumberFormatException was expected");
		assertEquals("For input string: \"One\"", thrown.getMessage());
	}

	@Test
	@DisplayName("La m�thode parseInt peut aussi lever de fa�on moins pr�cise une  IllegalFormatException")
	void testExpectedExceptionWithParentType() {
		assertThrows(IllegalArgumentException.class, () -> {
			Integer.parseInt("One");
		});
	}
	
	@Test
	@DisplayName("Que se passe t'il dans ce cas l� ?")
	void testExpectedExceptionFail() {
		assertThrows(NumberFormatException.class, () -> {
			Integer.parseInt("1");
		}, "NumberFormatException error was expected");
	}
	
	@Test
	@DisplayName("Que se passe t'il dans ce cas l� ?")
	void testBadExceptionWithParentType() {
		assertThrows(IllegalFormatException.class, () -> {
			Integer.parseInt("One");
		});
	}	
}
