import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@DisplayName("Tests d'une classe Student")
class StudentTest {

	@Test
	@DisplayName("un seul test group� pour le  nom et pr�nom d'un �tudiant")
	void testStudent1() {
		Student student = new Student("Herv�", "Leblanc");

		// In a grouped assertion all assertions are executed, 
		// and any failures will be reported together.
		assertAll("student Herv� Leblanc", 
				() -> assertEquals("Herv�", student.getFirstName()),
				() -> assertEquals("Leblanc", student.getLastName())
				);
	}

	@Test
	@DisplayName("Si le pr�nom n'est pas renseign�, il devient unkown")
	void testStudent2() {
		Student student = new Student(null, "Leblanc");
		
		// Within a code block, if an assertion fails the
		// subsequent code in the same block will be skipped.
		assertAll("student avec pr�nom null",
				() -> {
					assertNotNull(student.getFirstName());
					// Executed only if the previous assertion is valid.
					assertAll("first name",
							() -> assertEquals("unkown", student.getFirstName()),
							() -> assertEquals("Leblanc", student.getLastName())
							);
				}
		);
	}
}