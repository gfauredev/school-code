<?php
require_once 'jwt_utils.php';

// Hard-coded credentials
$login = 'user';
$password = 'password';

// If good credentials, send jwt
if ($body['login'] == $login && $body['password'] == $password) {
  $jwt = 'jwt';
  deliver_response(201, "Successfully authenticated", $jwt);
} else {
  deliver_response(400, "Wrong credentials, authentication failed");
}
break;
