<?php
class DbCrud
{
  private PDO $PDO;
  private array $tables;

  public function __construct(array $tables, string $name, string $pass = 'password', string $host = '127.0.0.1', string $type = 'mysql', string $user = null)
  {
    $this->tables = $tables;
    $this->PDO = new PDO($type . ':host=' . $host . ';dbname=' . $name, isset($user) ? $user : $name, $pass);
  }

  private function get_last_row(string $table)
  {
    return $this->PDO->query('
      SELECT * FROM ' . $table
      . ' ORDER BY id DESC LIMIT 1')->fetchAll(PDO::FETCH_ASSOC);
  }


  // Parse query string into array of associatives
  // arrays with 'param' => 'value'
  private function parse_query(array $query)
  {
    // var_dump($query);

    $assoc_queries = [];
    for ($i = 0; $i < count($query) / 2; $i++) {
      $table = $query[$i * 2];
      if (in_array($table, $this->tables)) {
        $assoc_queries[$i] = ['table' => $query[$i * 2]];
      } else {
        throw new Exception("Ressource type don't exists");
      }

      if (isset($query[$i * 2 + 1])) {
        $assoc_queries[$i] += ['id' => $query[$i * 2 + 1]];
      }
    }
    // var_dump($assoc_queries); // Debug
    // die(); // Debug

    return $assoc_queries;
  }

  // Test if queried ressources exists & return parsed query
  private function check_query(array $query)
  {
    $q = $this->parse_query($query);

    foreach ($q as $i) {
      if (!$this->get([$i['table'], $i['id']])) {
        throw new Exception("Ressource don't exists");
      }
    }

    return $q;
  }

  private static function query_to_sql_tables(array $q)
  {
    // Get tables comma separated string 
    // & WHERE selectors such as table = :table AND … string
    $tables = '';
    foreach ($q as $i) {
      $tables .= $i['table'] . ',\n';
    }
    // var_dump($tables);

    return substr($tables, 0, -3); // Remove last ,\n
  }

  private static function query_to_sql_wheres(array $q)
  {
    // Get tables comma separated string 
    // & WHERE selectors such as table = :table AND … string
    $selectors = '';
    foreach ($q as $i) {
      if (isset($i['id']))
        $selectors .= $i['table'] . '.id = :' . $i['table'] . ' AND\n';
    }
    // var_dump($selectors);

    if (strlen($selectors) > 1) {
      // Remove last AND
      return 'WHERE ' . substr($selectors, 0, -6);
    } else {
      return '';
    }
  }

  private static function body_to_sql_columns_assoc_params(array $body)
  {
    $columns = '';
    foreach ($body as $column => $value) {
      $columns .= $column . ' = :' . $column . ',';
    }

    return substr($columns, 0, -1); // Remove trailing ,
  }

  public function post(array $query = [], array $body = [])
  {
    // var_dump($body);
    $full_query = $this->parse_query($query);
    $q = end($full_query);
    // TODO handle inserting in multiple tables

    $columns = '';
    $columnsParams = '';
    foreach ($body as $column => $value) {
      $columns .= $column . ',';
      $columnsParams .= ':' . $column . ',';
    }
    $columns = substr($columns, 0, -1); // Remove last ,
    $columnsParams = substr($columnsParams, 0, -1); // Remove last ,
    // var_dump($columns);
    // var_dump($columnsParams);

    $req = $this->PDO->prepare('
      INSERT INTO ' . $q['table'] . ' (' . $columns
      . ') VALUES (' . $columnsParams . ')');
    // var_dump($req);

    foreach ($body as $column => $value) {
      $req->bindParam($column, $value);
      // var_dump($column);
      // var_dump($value);
    }

    if (!$req->execute())
      throw new Exception("Request failed");

    return $this->get_last_row($q['table']);
  }

  public function get(array $query = [])
  {
    $q = $this->parse_query($query);
    $tables = self::query_to_sql_tables($q);
    $selectors = self::query_to_sql_wheres($q);

    $req = $this->PDO->prepare('
      SELECT * FROM ' . $tables . ' ' . $selectors);

    if (strlen($selectors) > 1) {
      foreach ($q as $i) {
        $req->bindParam($i['table'], $i['id']);
      }
    }
    // var_dump($req);

    if (!$req->execute())
      throw new Exception("Request failed");

    // Return data or false if no rows
    $data = $req->fetchAll(PDO::FETCH_ASSOC);
    return count($data) > 0 ? $data : false;
  }

  public function patch(array $query, array $body)
  {
    return $this->put($query, $body);
  }

  public function put(array $query, array $body)
  {
    // var_dump($body);
    $full_query = $this->parse_query($query);
    $q = end($full_query);
    // TODO handle inserting in multiple tables

    $columns = self::body_to_sql_columns_assoc_params($body);
    // var_dump($columns);

    $req = $this->PDO->prepare('
      UPDATE ' . $q['table'] . ' SET ' . $columns
      . ' WHERE id=:id');

    foreach ($body as $column => $value) {
      $req->bindParam($column, $value);
    }
    $req->bindParam('id', $q['id']);
    // var_dump($req);

    if (!$req->execute())
      throw new Exception("Request failed");

    return $this->get([$q['table'], $q['id']]);
  }

  public function delete(array $query)
  {
    $q = $this->check_query($query)[0];

    $req = $this->PDO->prepare('
      DELETE FROM ' . $q['table'] . '
      WHERE id=:id');
    $req->bindParam('id', $q['id']);
    // var_dump($req);

    if (!$req->execute())
      throw new Exception("Request failed");

    return $q;
  }
}
