<?php
function deliver_response($status = 200, $status_message = null, $data = null)
{
  // Set HTTP header with status
  header("HTTP/1.1 $status $status_message");

  // Set HTTP response body with message & data
  $response['status'] = $status;
  if (isset($status_message))
    $response['status_message'] = $status_message;
  if (isset($data))
    $response['data'] = $data;

  // Return the response as JSON & end the script
  die(json_encode($response));
}

// Instantiate the db connexion with CRUD operations
require_once 'DbCrud.php';
$db = new DbCrud(['facts', 'test'], 'rest_test');

// Set HTTP header to json
header('Content-Type:application/json');

// Store query string & body JSON data in arrays
// if (!empty($_GET['query']))
$query = explode('/', $_GET['query']);
$body = file_get_contents('php://input');
$body = json_decode($body, true);

// Deliver proper response to client according to HTTP method
switch ($_SERVER['REQUEST_METHOD']) {
  case 'POST':
    try {
      $res = $db->post($query, $body);
      $id = $res[0]['id'];
      deliver_response(201, "Successfully created ressource $id", $res);
    } catch (Exception $e) {
      deliver_response(400, 'Failed to create ressource, provided data may be incorrect');
    }
    break;
  case 'PATCH':
    try {
      $res = $db->patch($query, $body);
      $id = $res[0]['id'];
      deliver_response(200, "Successfully patched ressource $id", $res);
    } catch (Exception $e) {
      deliver_response(410, "Failed to patch ressource, provided ID may be wrong");
    }
    break;
  case 'PUT':
    try {
      $res = $db->put($query, $body);
      $id = $res[0]['id'];
      deliver_response(200, "Successfully updated ressource $id", $res);
    } catch (Exception $e) {
      deliver_response(410, 'Failed to update ressource, provided ID may be wrong');
    }
    break;
  case 'DELETE':
    try {
      $res = $db->delete($query);
      $table = $res['table'];
      $id = $res['id'];
      deliver_response(204, "Successfully deleted $table/$id");
    } catch (Exception $e) {
      deliver_response(410, 'Failed to delete ressource, provided ID may be wrong');
    }
    break;
  default:
    $res = $db->get($query);
    if ($res) {
      deliver_response(200, 'Data sucessfully retrieved',$res);
    } else {
      deliver_response(404, 'Data not found');
    }
    break;
}
