import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class TestDistributeurAuto {
	private Distributeur d;
	List<Couple> p;
	private int f;
	private int m;
	private int n;
	private int r;

	@Before
	public void setUp() throws Exception {
		// On crée et recharge le distributeur
		this.d = new Distributeur(0, 0, 0);
		this.d.recharger(10, 10, 10);
	}

	@After
	public void tearDown() throws Exception {
		// On supprimer les variables
		this.d = null;
		this.p = null;
	}
	
	@Parameters(name = "Test retrait {1}€, {2} billets, {3}€ restant, premier de {4}")
	public static Collection<Object[]> parameters() {
		return  Arrays.asList( new Object[][] {
			{10, 10, 1, 0}, {10, 20, 2, 0}, {20, 30, 1, 0},
			{20, 40, 1, 0}, {20, 50, 2, 0}, {20, 60, 2, 0},
			{20, 70, 3, 0}, {50, 100, 1, 0}, {50, 110, 1, 0},
			{50, 210, 2, 0}, {50, 310, 3, 0}, {50, 3000, 10, 2200}
		});
	}
	
	public TestDistributeurAuto(int premierBillet,
			int montantRetrait, int nombrePremierBillets, int restantDu) {
		this.f = premierBillet;
		this.m = montantRetrait;
		this.n = nombrePremierBillets;
		this.r = restantDu;
	}

	@Test
	public void testRetraitParam() {
		p = d.donnerBillets(m);
		System.out.println(p.toString());
		System.out.println(d.toStringProposition(p, m));
		assertEquals(r,d.montantRestantDu(p, m));
		assertEquals(f,p.get(0).getValeurBillet());
		assertEquals(n,p.get(0).getNombreBilletsDelivres());
	}
}