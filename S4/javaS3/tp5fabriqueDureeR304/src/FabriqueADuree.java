
public interface FabriqueADuree {
	public abstract Duree create(int heures, int minutes, int secondes);
}
