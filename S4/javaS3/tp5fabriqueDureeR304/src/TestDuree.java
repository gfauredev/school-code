import static org.junit.Assert.*;

import org.junit.Test;


public abstract class TestDuree {

	@Test
	public void testGetters() {
		Duree d = Duree(1, 2, 3);
		assertEquals(1,d.getHeures());
		assertEquals(2,d.getMinutes());
		assertEquals(3,d.getSecondes());
	}

	@Test (expected=IllegalArgumentException.class)
	public void testHeuresNegative() {
		Duree(-1, 2, 3);
	}
	
	@Test (expected=IllegalArgumentException.class)
	public void testMinutesNegative() {
		Duree(1, -2, 3);
	}
	
	@Test (expected=IllegalArgumentException.class)
	public void testMinutesSuperieur59() {
		Duree(1, 60, 3);
	}
	
	@Test (expected=IllegalArgumentException.class)
	public void testSecondesNegative() {
		Duree(1, 2, -3);
	}
	
	@Test (expected=IllegalArgumentException.class)
	public void testSecondesSuperieur59() {
		Duree(1, 2, 60);
	}
	
	@Test
	public void testEqualsValeursEgales() {
		assertEquals(Duree(1,2,3),Duree(1,2,3));
	}
	
	@Test
	public void testEqualsValeursNonEgales() {
		assertNotEquals(Duree(1,2,3),Duree(2,2,3));
		assertNotEquals(Duree(1,2,3),Duree(1,1,3));
		assertNotEquals(Duree(1,2,3),Duree(1,2,2));
	}
	
	@Test
	public void testAjouterUneSeconde() {
		Duree d123 = Duree(1,2,3);
		d123.ajouterUneSeconde();
		assertEquals(Duree(1,2,4),d123);
		
		Duree d1259 = Duree(1,2,59);
		d1259.ajouterUneSeconde();
		assertEquals(Duree(1,3,0),d1259);
		
		Duree d5959 = Duree(1,59,59);
		d5959.ajouterUneSeconde();
		assertEquals(Duree(2,0,0),d5959);
	}
	
	@Test
	public void testCompareTo() {
		assertTrue(Duree(1,2,4).compareTo(Duree(1,0,0)) > 0);
		assertEquals(0,Duree(1,2,4).compareTo(Duree(1,2,4)));
		assertTrue(Duree(1,0,0).compareTo(Duree(1,2,4)) < 0);
	}
	
	@Test 
	public void testToString() {
		assertEquals("1:2:3", Duree(1,2,3).toString());
	}
}
