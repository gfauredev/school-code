import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

public class ListeCirculaire<T> {
	private List<T> list = new LinkedList<T>();
	ListIterator<T> iterator;

	public ListeCirculaire() {
		this.iterator = null;
	}
	
	public void ajouter(T element) {
		this.list.add(element);
	}
	
	private void estVide() throws ListeVide {
		if (this.list.size() == 0) {
			throw new ListeVide("Impossible, la liste est vide");
		}
	}
	
	public T premier() throws ListeVide {
		this.estVide();
		this.iterator = this.list.listIterator();

		return this.iterator.next();
	}
	
	public T suivant() throws ListeVide {
		this.estVide();
		
		if (this.iterator.hasNext()) {
			return this.iterator.next();
		}
		return this.premier();
	}
}