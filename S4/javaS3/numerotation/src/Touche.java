public class Touche {
	private ListeCirculaire<Character> chars = new ListeCirculaire<Character>();
	
	public Touche(String chars) {
		for (Character c : chars.toCharArray()) {
			this.chars.ajouter(c);
		}
	}
	
	public Character getCaractère(int nb) throws ListeVide {
		this.chars.premier();
		for (int i = 0; i < nb - 1; i++) {
			this.chars.suivant();
		}
		
		return this.chars.suivant();
	}
}