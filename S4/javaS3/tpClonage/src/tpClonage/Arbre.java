package tpClonage;

import java.util.Objects;

public class Arbre extends ObjetGraphique implements Cloneable {
	private int hauteur;
	private String couleurTronc;
	private String couleurFeuilles;
	
	public Arbre(double coordX, double coordY, int hauteur,
			String couleurTronc, String couleurFeuilles) {
		super(coordX, coordY);
		this.hauteur = hauteur;
		this.couleurTronc = couleurTronc;
		this.couleurFeuilles = couleurFeuilles;
	}
	
	public void setHauteur(int hauteur) {
		this.hauteur = hauteur;
	}
	
	public void setCouleurTronc(String couleur) {
		this.couleurTronc = couleur;
	}
	
	public void setCouleurFeuilles(String couleur) {
		this.couleurFeuilles = couleur;
	}
	
	public String toString() {
		return super.toString() + "\nHauteur = " + this.hauteur
				+ "\nCouleur tronc = " + this.couleurTronc
				+ "\nCouleur feuilles = " + this.couleurFeuilles;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(couleurFeuilles, couleurTronc, hauteur);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Arbre other = (Arbre) obj;
		return Objects.equals(couleurFeuilles, other.couleurFeuilles)
				&& Objects.equals(couleurTronc, other.couleurTronc) && hauteur == other.hauteur;
	}

	public Arbre clone() {
		Arbre clone = null;
			clone = (Arbre) super.clone();
		return clone;
	}

}
