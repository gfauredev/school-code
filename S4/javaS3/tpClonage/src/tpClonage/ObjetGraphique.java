package tpClonage;

import java.util.Objects;

public class ObjetGraphique implements Cloneable {
	private double coordX;
	private double coordY;
	
	public ObjetGraphique(double coordX, double coordY) {
		this.coordX = coordX;
		this.coordY = coordY;
	}
	
	public void translation(double dx, double dy) {
		this.coordX += dx;
		this.coordY += dy;
	}
	
	public String toString() {
		return("X = " + this.coordX + "\nY = " + this.coordY);
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(coordX, coordY);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ObjetGraphique other = (ObjetGraphique) obj;
		return Double.doubleToLongBits(coordX) == Double.doubleToLongBits(other.coordX)
				&& Double.doubleToLongBits(coordY) == Double.doubleToLongBits(other.coordY);
	}

	public ObjetGraphique clone() {
		ObjetGraphique clone = null;
		try {
			clone = (ObjetGraphique) super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return clone;
	}

}
