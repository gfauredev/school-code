package tpClonage;

import java.util.Objects;

public class Balançoire extends ObjetGraphique implements Cloneable {
	private int places;
	private String couleur;
	
	public Balançoire(double coordX, double coordY, int places,
			String couleur) {
		super(coordX, coordY);
		this.places = places;
		this.couleur = couleur;
	}
	
	public void setplaces(int places) {
		this.places = places;
	}
	
	public void setcouleur(String couleur) {
		this.couleur = couleur;
	}
	
	public String toString() {
		return super.toString() + "\nplaces = " + this.places
				+ "\nCouleur tronc = " + this.couleur;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(couleur, places);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Balançoire other = (Balançoire) obj;
		return Objects.equals(couleur, other.couleur) && places == other.places;
	}

	public Balançoire clone() {
		Balançoire clone = null;
			clone = (Balançoire) super.clone();
		return clone;
	}
}
