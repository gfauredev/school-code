package tpClonage;

import java.util.Objects;

public class Immeuble extends ObjetGraphique implements Cloneable {
	private String couleur;
	private int nbEtages;
	private int hauteurEtage;
	
	public Immeuble(double coordX, double coordY, String couleur,
			int nbEtages, int hauteurEtage) {
		super(coordX, coordY);
		this.couleur = couleur;
		this.nbEtages=nbEtages;
		this.hauteurEtage=hauteurEtage;
	}
	
	public void setCouleur(String couleur) {
		this.couleur=couleur;
	}
	
	public void setNbEtages(int nbEtages) {
		this.nbEtages=nbEtages;
	}
	
	public void setHauteurEtage(int hauteurEtage) {
		this.hauteurEtage=hauteurEtage;
	}
	
	public String toString() {
		return super.toString() + "\nCouleur = " + this.couleur
				+ "\n" + this.nbEtages + " étages, de "
				+ this.hauteurEtage + " m";
	}
	
	public Immeuble clone() {
		Immeuble clone = null;
			clone = (Immeuble) super.clone();
		return clone;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(couleur, hauteurEtage, nbEtages);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Immeuble other = (Immeuble) obj;
		return Objects.equals(couleur, other.couleur) && hauteurEtage == other.hauteurEtage
				&& nbEtages == other.nbEtages;
	}
}
