package tpClonage;

import java.util.Objects;

public class Banc extends ObjetGraphique implements Cloneable {
	private int places;
	private String couleur;
	
	public Banc(double coordX, double coordY, int places,
			String couleur) {
		super(coordX, coordY);
		this.places = places;
		this.couleur = couleur;
	}
	
	public void setPlaces(int places) {
		this.places = places;
	}
	
	public void setcouleur(String couleur) {
		this.couleur = couleur;
	}
	
	public String toString() {
		return super.toString() + "\nplaces = " + this.places
				+ "\nCouleur tronc = " + this.couleur;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(couleur, places);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Banc other = (Banc) obj;
		return Objects.equals(couleur, other.couleur) && places == other.places;
	}

	public Banc clone() {
		Banc clone = null;
			clone = (Banc) super.clone();
		return clone;
	}
}
