package tpClonage;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestImmeuble {

	@Test
	public void testClonageImmeuble() {
		Immeuble i1 = new Immeuble(123,234,"cyan",3,4);
		Immeuble i2 = i1.clone();
		
		assertNotSame(i1,i2);
		assertEquals(i1,i2);
	}

}
