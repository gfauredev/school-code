package tpClonage;

import java.util.Objects;

public class Toboggan extends ObjetGraphique implements Cloneable {
	private int longueur;
	private String couleur;
	
	public Toboggan(double coordX, double coordY, int longueur,
			String couleur) {
		super(coordX, coordY);
		this.longueur = longueur;
		this.couleur = couleur;
	}
	
	public void setlongueur(int longueur) {
		this.longueur = longueur;
	}
	
	public void setcouleur(String couleur) {
		this.couleur = couleur;
	}
	
	public String toString() {
		return super.toString() + "\nlongueur = " + this.longueur
				+ "\nCouleur tronc = " + this.couleur;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(couleur, longueur);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Toboggan other = (Toboggan) obj;
		return Objects.equals(couleur, other.couleur) && longueur == other.longueur;
	}

	public Toboggan clone() {
		Toboggan clone = null;
			clone = (Toboggan) super.clone();
		return clone;
	}
}
