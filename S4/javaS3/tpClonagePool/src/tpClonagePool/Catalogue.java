package tpClonagePool;

import tpClonage.*;

public class Catalogue {
	private static Catalogue instance;
	
	private final Immeuble protoImmeuble = new Immeuble(1,1,"anthracite",6,4);
	private final Arbre protoArbre = new Arbre(1,1,7,"marron","vert");
	private final Balançoire protoBalançoire = new Balançoire(1,1,3,"orange");
	private final Banc protoBanc = new Banc(1,1,4,"beige");
	private final Toboggan protoToboggan = new Toboggan(1,1,5,"argent");
	private final Tourniquet protoTourniquet = new Tourniquet(1,1,8,"bleu");

	private Catalogue() {}
	public static synchronized Catalogue get()
	{
		if (instance == null)
			instance = new Catalogue();

		return instance;
	}
	
	public Immeuble getUnImmeuble() {
		return protoImmeuble.clone();
	}
	
	public Arbre getUnArbre() {
		return protoArbre.clone();
	}

	public Balançoire getUneBalançoire() {
		return protoBalançoire.clone();
	}

	public Banc getUnBanc() {
		return protoBanc.clone();
	}

	public Toboggan getUnToboggan() {
		return protoToboggan.clone();
	}

	public Tourniquet getUnTourniquet() {
		return protoTourniquet.clone();
	}

	@Override
	public String toString() {
		return "Catalogue [protoImmeuble=" + protoImmeuble + ", protoArbre=" + protoArbre + ", protoBalançoire="
				+ protoBalançoire + ", protoBanc=" + protoBanc + ", protoToboggan=" + protoToboggan
				+ ", protoTourniquet=" + protoTourniquet + "]";
	}
}
