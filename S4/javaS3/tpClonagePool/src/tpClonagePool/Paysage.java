package tpClonagePool;

import tpClonage.*;

public class Paysage {

	public static void main(String[] args) {
		Catalogue catalogue = Catalogue.get();

		ObjetGraphique[] paysage = {catalogue.getUnImmeuble(),
				catalogue.getUnArbre(),
				catalogue.getUnBanc(),
				catalogue.getUneBalançoire(),
				catalogue.getUnToboggan(),
				catalogue.getUnTourniquet()};

		for (ObjetGraphique obj : paysage) {
			obj.translater(Math.random() * 10, Math.random() * 10);
			System.out.println(obj);
		}
	}
}