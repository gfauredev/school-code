import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

public class JeuDeCartes {
	private List<Carte> cartes = new ArrayList<Carte>();
	
	public JeuDeCartes() {
		Couleurs [] tabCouleurs = Couleurs.values();
		Valeurs [] tabValeurs = Valeurs.values();

		for (Couleurs couleur : tabCouleurs) {
			for (Valeurs valeur : tabValeurs) {
				this.cartes.add(new Carte(valeur,couleur));
			}
		}
	}
	
	public void classerPaquet() {
		Collections.sort(this.cartes);
	}
	
	public void mélangerPaquet() {
		Collections.shuffle(this.cartes);
	}
	
	public String retournerPremiereCarte() {
		Collections.rotate(cartes, -1);
		return this.cartes.get(this.cartes.size()-1).toString();
	}
	
	public Carte distribuerCarte() {
		return this.cartes.remove(0);
	}
	
	public String toString() {
		String jeu = new String();
		
		for (Carte carte : this.cartes) {
			jeu = jeu.concat(carte.toString() + System.lineSeparator());
		}
		
		return jeu;
	}
}