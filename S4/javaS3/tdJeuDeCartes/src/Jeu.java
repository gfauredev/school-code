public class Jeu {
	public static void main(String[] args) {
		JeuDeCartes jeu = new JeuDeCartes();
		
		System.out.println("Paquet généré :");
		System.out.println(jeu);
		
		jeu.mélangerPaquet();
		System.out.println("Paquet mélangé :");
		System.out.println(jeu);
		
		jeu.classerPaquet();
		System.out.println("Paquet classé :");
		System.out.println(jeu);
		
		System.out.println("Retournement de la carte " + jeu.retournerPremiereCarte());
		System.out.println(jeu);
		
		jeu.classerPaquet();
		System.out.println("Paquet classé :");
		System.out.println(jeu);
		
		System.out.println("Distribution de la carte " + jeu.distribuerCarte());
		System.out.println(jeu);

		jeu.classerPaquet();
		System.out.println("Paquet classé :");
		System.out.println(jeu);
		
	}
}