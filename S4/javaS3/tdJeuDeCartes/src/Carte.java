public class Carte implements Comparable<Carte> {
	private Valeurs valeur;
	private Couleurs couleur;
	
	public Carte(Valeurs valeur, Couleurs couleur) {
		this.valeur = valeur;
		this.couleur = couleur;
	}
	
	public Valeurs getValeur() {
		return this.valeur;
	}
	
	public Couleurs getCouleur() {
		return this.couleur;
	}
	
	public String toString() {
		return new String(this.getValeur() + " OF " + this.getCouleur());
	}
	
	public int compareTo(Carte autre) {
		if (this.getCouleur() == autre.getCouleur()) {
			return this.getValeur().ordinal() - autre.getValeur().ordinal();
		} else {
			return this.getCouleur().ordinal() - autre.getCouleur().ordinal();
		}
	}
}