public class Entier implements ExpressionArithmetique {
	private int value;
	
	public Entier(int value) {
		this.value = value;
	}
	
	public int evaluate() {
		return this.value;
	}
}