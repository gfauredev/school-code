import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class testExpressionPlus {
	private ExpressionArithmetique testedExpression;

	@Before
	public void setUp() throws Exception {
		this.testedExpression = new ExpressionPlus();
	}

	@After
	public void tearDown() throws Exception {
		this.testedExpression = null;
	}

	@Test
	public void testAdd() {
		final int[][] testValues = { { 1 }, { 5, 7 }, { 1, 2, -3 } };

		for (int[] values : testValues) {
			int res = 0;

			for (int value : values) {
				res += value;
				testedExpression.add(value);
			}

			assertEquals(testedExpression.evaluate(), res);
		}
	}

}
