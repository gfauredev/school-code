import java.util.ArrayList;
import java.util.List;

public class OperationArithmetique implements ExpressionArithmetique {
	protected List<ExpressionArithmetique> expressions = new ArrayList<>();
	
	public int evaluate() {
		int result = 0;
		
		for (ExpressionArithmetique e: this.expressions) {
			result += e.evaluate();
		}
		
		return result;
	}
}