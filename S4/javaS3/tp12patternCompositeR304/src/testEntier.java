import static org.junit.Assert.*;
import org.junit.Test;

public class testEntier {
	@Test
	public void testConstructValues() {
		final int[] testValues = {7, 5, 3, 2, 1, 0, -1, -2, -3, -5, -7};

		for (int testValue: testValues) {
			ExpressionArithmetique testedValue = new Entier(testValue);
			assertEquals(testedValue.evaluate(), testValue);
		}
	}
}