
public class Loterie {
	
	private int[] boulesTirées;

	public Loterie() {
		this.boulesTirées = new int[60];
	}
	
	public String boulesSorties() {
		String res = "";
		for (int i = 0; i < this.boulesTirées.length; i++) {
			if (bouleTirée(i))
				if (res.equals(""))
					res += i;
				else
					res = res + " " + i;
		}
		return res;
	}

	private boolean bouleTirée(int valeur) {
		return this.boulesTirées[valeur] > 0;
	}

	public void addBoule(int valeur) {
		this.boulesTirées[valeur]+=1;
	}
}
