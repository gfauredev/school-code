public class DateTirage {
	
	private int jour;
	private int mois;
	private int ann�e;
	private String toString;
	
	public static DateTirage premierOctobre = new DateTirage(1,10,2021, "Vendredi 1er Octobre 2021");
	public static DateTirage quatreOctobre = new DateTirage(4,10,2021, "Lundi 4 Octobre 2021");
	public static DateTirage sixOctobre = new DateTirage(6,10,2021, "Mercredi 6 Octobre 2021");
	public static DateTirage huitOctobre = new DateTirage(8,10,2021, "Vendredi 8 Octobre 2021");
	public static DateTirage OnzeOctobre = new DateTirage(11,10,2021, "Lundi 11 Octobre 2021");
	public static DateTirage TreizeOctobre = new DateTirage(13,10,2021, "Mercredi 13 Octobre 2021");
	public static DateTirage QuinzeOctobre = new DateTirage(15,10,2021, "Vendredi 15 Octobre 2021");
	public static DateTirage vingtcinqOctobre = new DateTirage(25,10,2021, "Lundi 25 Octobre 2021");
	public static DateTirage vingtseptOctobre = new DateTirage(27,10,2021, "Mercredi 27 Octobre 2021");
	public static DateTirage vingtneufOctobre = new DateTirage(29,10,2021, "Vendredi 29 Octobre 2021");
	
	private DateTirage(int jour, int mois, int ann�e, String toString) {
		this.jour = jour;
		this.mois = mois;
		this.ann�e = ann�e;
		this.toString = toString;
	}

	public String toString() {
		return "Date tirage : " + this.toString;
	}
	

}
