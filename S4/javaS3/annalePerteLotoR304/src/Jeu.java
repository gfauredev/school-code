public class Jeu {
	
	private Joueur unJoueur;
	private Grille uneGrille;
	private DateTirage uneDateDeTirage;
	
	public Jeu(Joueur unJoueur, Grille uneGrille, DateTirage uneDateDeTirage) {
		this.unJoueur = unJoueur;
		this.uneGrille = uneGrille;
		this.uneDateDeTirage = uneDateDeTirage;
	}
	
	public String toString() {
		return unJoueur.toString() + '\n' +  uneDateDeTirage.toString() + '\n' +  uneGrille.toString() + '\n' + "Prix � payer : " + uneGrille.prix() + " �";
	}
	
}
