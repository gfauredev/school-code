
public class Email {
	
	private static final String AT = " at ";
	
	private String identifiant;
	private String organisation;
	
	public Email(String identifiant, String organisation) {
		this.identifiant = identifiant;
		this.organisation = organisation;
	}

	@Override
	public String toString() {
		return "Email : " + identifiant + AT + organisation;
	}

}
