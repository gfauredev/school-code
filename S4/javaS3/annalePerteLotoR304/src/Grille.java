import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Grille {
	
	private static float[][] prixGrilleMultiple = {{ 2.2F,  13.2F,  46.2F, 123.2F, 277.2F},
												   { 4.4F,  26.4F,  92.4F, 246.4F, Float.MAX_VALUE},
												   { 6.6F,  39.6F, 138.6F, 369.6F, Float.MAX_VALUE},
												   { 8.8F,  52.8F, 184.8F, Float.MAX_VALUE, Float.MAX_VALUE},
												   {  11F,  66.0F, 231.0F, Float.MAX_VALUE, Float.MAX_VALUE},
												   {13.2F,  79.2F, 277.2F, Float.MAX_VALUE, Float.MAX_VALUE},
												   {15.4F,  92.4F, 323.4F, Float.MAX_VALUE, Float.MAX_VALUE},
												   {17.6F, 105.6F, 369.6F, Float.MAX_VALUE, Float.MAX_VALUE},
												   {19.8F, 118.8F, Float.MAX_VALUE, Float.MAX_VALUE, Float.MAX_VALUE},
												   {22.0F, 132.0F, Float.MAX_VALUE, Float.MAX_VALUE, Float.MAX_VALUE}};
	
	private List<Integer> num�rosGrille = new LinkedList<Integer>();
	private List<Integer> num�rosCompl�mentaires = new LinkedList<Integer>();
	
	public void updateGrilleNumero(Integer... num�rosGrille) throws IllegalArgumentException {
		if (num�rosGrille.length < 5 || num�rosGrille.length > 9)
			throw new IllegalArgumentException("Vous devez saisir entre 5 et 9 num�ros");
		Arrays.sort(num�rosGrille);
		if (deuxNum�rosIdentiques(num�rosGrille))
			throw new IllegalArgumentException("Pas deux fois le m�me num�ro !");
		if (auMoinsUnNum�roHorsLimite(1, 49, num�rosGrille))
			throw new IllegalArgumentException("Num�ros d'une grille compris entre 1 et 10");
		this.num�rosGrille = Arrays.asList(num�rosGrille);
	}
	
	public void updateGrilleCompl�mentaire(Integer... num�rosCompl�mentaires) {
		if (num�rosCompl�mentaires.length < 1 || num�rosCompl�mentaires.length > 10)
			throw new IllegalArgumentException("Vous devez saisir entre 1 et 10 num�ros compl�mentaires");
		Arrays.sort(num�rosCompl�mentaires);
		if (deuxNum�rosIdentiques(num�rosCompl�mentaires))
			throw new IllegalArgumentException("Pas deux fois le m�me num�ro !");
		if (auMoinsUnNum�roHorsLimite(1, 10, num�rosCompl�mentaires))
			throw new IllegalArgumentException("Num�ros compl�mentaires compris entre 1 et 10");
		this.num�rosCompl�mentaires = Arrays.asList(num�rosCompl�mentaires);
	}
	
	private static boolean deuxNum�rosIdentiques(Integer... listeNum�ros) {
		for (int i = 1; i < listeNum�ros.length; i++) {
			if (listeNum�ros[i] == listeNum�ros[i-1])
				return true;
		}
		return false;
	}
	
	private static boolean auMoinsUnNum�roHorsLimite(int plusPetit, int plusGrand, Integer... listeNum�ros) {
		return (listeNum�ros[0] < plusPetit || listeNum�ros[listeNum�ros.length-1] > plusGrand);
	}
	
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("grille : ");
		for(Integer num�ro : this.num�rosGrille) {
			sb.append(num�ro.toString());
			sb.append(' ');
		}
		sb.append("num�ro(s) compl�mentaires : ");
		for(Integer num�ro : this.num�rosCompl�mentaires) {
			sb.append(num�ro.toString());
			sb.append(' ');
		}
		return sb.toString();
	}
	
	public float prix() {
		return prixGrilleMultiple[num�rosGrille.size()-5][num�rosCompl�mentaires.size()-1];
	}
}
