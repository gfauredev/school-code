
public class Joueur {
	
	private String nom;
	private String pr�nom;
	private Email email;
	
	public Joueur(String nom, String pr�nom, Email email) {
		this.nom = nom;
		this.pr�nom = pr�nom;
		this.email = email;
	}

	@Override
	public String toString() {
		return "Joueur : "+ nom + ' ' +  pr�nom + ", " + email;
	}

}
