package tpTableChevaliers;

public class Convive {
	private enum CIVILITE {M,MME};

	private String nom;
	private String prenom;
	private CIVILITE civilite;
	
	public Convive(String nom, String prenom, CIVILITE civilite) {
		this.nom = nom;
		this.prenom = prenom;
		this.civilite = civilite;
	}
	
	public String getNom() {
		return this.nom;
	}
	
	public String getPrenom() {
		return this.prenom;
	}
	
	public String getDescription() {
		return "(" + this.civilite + ") " 
			+ this.prenom + " " + this.nom;
	}
	
	public String toString() {
		return this.getDescription();
	}
	
	public int hashCode() {
		return this.toString().hashCode();
	}

}
