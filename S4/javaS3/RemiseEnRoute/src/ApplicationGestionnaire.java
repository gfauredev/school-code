public class ApplicationGestionnaire {

		public static void main(String a[])
		{
			// Création du gestionnaire
			GestionnaireApplications gestionnaire = new GestionnaireApplications(400);
							
			// Création d'applications et ajout au gestionnaire		
			Application app1 = new App_Noyau("app#1", 10, Application.Domaines.ORGANISATION);	
			App_Noyau app2 = new App_Noyau("app#2", 124, Application.Domaines.BUREAUTIQUE);
			App_Graphique app3 = new App_Graphique("app#3",354, Application.Domaines.JEU);
			App_Utilisateur app4 = new App_Utilisateur("app#4",256, Application.Domaines.DÉVELOPPEMENT);
					
			// ajout au gestionnaire
			gestionnaire.installer(app4);
			gestionnaire.installer(app1);
			gestionnaire.installer(app2);
			gestionnaire.installer(app3);
		
				
			gestionnaire.desinstaller(app3);
			gestionnaire.desinstaller(app4);		
		}	
}
