interface Desinstallable
{
	// Méthode permettant de désinstaller une application
	public abstract void désinstaller() throws Exception;
}


abstract class Application 
{
	private String nom; // Nom de l'application
	private int nbBlocs ; // Taille de l'application en blocs
	
	enum Domaines {JEU,BUREAUTIQUE,DÉVELOPPEMENT,ORGANISATION};
	private Domaines domaine;
	
	
	/**
	 * Création d'une Application
	 * @param nom de l'application
	 * @param nbBlocs nécessaires pour installer l'application
	 */
	public Application(String nom, int nbBlocs, Domaines domaine) {
		this.nbBlocs = nbBlocs;
		this.nom = nom;
		this.domaine = domaine;
	}


	/**
	 * @return le nom de l'application
	 */
	public String getNom() {
		return nom;
	}


	/**
	 * @return le nombre de blocs nécessaires pour installer l'application
	 */
	public int getNbBlocs() {
		return nbBlocs;
	}	
	
	/**
	 * @return domaine de l’application
	 */
	public Domaines getDomaine() {
		return domaine;
	}
	
	/**
	 * @return description de l’application de la forme
	 * [NOM] - [DOMAINE] ([TAILLE])
	 */
	public String toString() {
		return new String(this.getNom() + " - " +
						  this.getDomaine() + " (" +
						  this.getNbBlocs() + ")");
	}
}


abstract class App_Système extends Application
{
	/**
	 * Création d'une Application Système
	 * @param nom de l'application
	 * @param nbBlocs nécessaires pour installer l'application
	 */
	public App_Système(String nom, int nbBlocs, Domaines domaine) {
		super(nom, nbBlocs, domaine);		
	}
}



class App_Noyau extends App_Système
{
	/**
	 * Création d'une Application Noyau
	 * @param nom de l'application
	 * @param nbBlocs nécessaires pour installer l'application
	 */
	public App_Noyau(String nom, int nbBlocs, Domaines domaine) {
		super(nom, nbBlocs, domaine);		
	}

}

class App_Utilisateur extends Application implements Desinstallable
{
	/**
	 * Création d'une Application Utilisateur (désinstallable)
	 * @param nom de l'application
	 * @param nbBlocs nécessaires pour installer l'application
	 */
	public App_Utilisateur(String nom, int nbBlocs, Domaines domaine) {
		super(nom, nbBlocs, domaine);
	}

	/**
	 * Désinstaller l'application. Le pb se déclenche au hasard
	 * Implantation de l'interface Désinstallable 
	 */
	public void désinstaller()  throws Exception
	{
		if (Math.random() >= 0.5) {
			throw new Exception ("Une erreur s'est produite lors de la désinstallation...");
		}
	}
}

class App_Graphique extends App_Système implements Desinstallable
{
	/**
	 * Création d'une Application Utilisateur (désinstallable)
	 * @param nom de l'application
	 * @param nbBlocs nécessaires pour installer l'application
	 */
	public App_Graphique(String nom, int nbBlocs, Domaines domaine) {
		super(nom, nbBlocs, domaine);		
	}
	
	/**
	 * Désinstaller l'application. Le pb se déclenche au hasard
	 * Implantation de l'interface Désinstallable 
	 */
	public void désinstaller()  throws Exception
	{
		if (Math.random() >= 0.5) {
			throw new Exception ("Une erreur s'est produite lors de la désinstallation...");
		}
	}
}