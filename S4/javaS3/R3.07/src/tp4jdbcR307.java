import java.sql.*;
import oracle.jdbc.pool.OracleDataSource;

public class tp4jdbcR307 {

	public static void main(String[] args) throws SQLException {
		OracleDataSource bd = new CictOracleDataSource();
		
		Connection cn = bd.getConnection();

		Statement stEns = cn.createStatement();
		
		ResultSet rsEns = stEns.executeQuery(
				"SELECT nom, nbhdisp, grade FROM enseignant"
				+ " WHERE grade IN ('MCF', 'PROF')"
				);
		
		while (rsEns.next()) {
			System.out.println(rsEns.getString("nom")
				+ " a dispensé "+ rsEns.getString("nbhdisp")
				+ " h d’enseignement EQTD");
		}
		
		cn.close();
		oracle.jdbc.datasource.impl.OracleDataSource.cleanup();
	}
}