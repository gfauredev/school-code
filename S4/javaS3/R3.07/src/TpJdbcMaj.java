import java.sql.*;
import oracle.jdbc.pool.OracleDataSource;

public class TpJdbcMaj {

	public static void main(String[] args) throws SQLException {
		OracleDataSource bd = new CictOracleDataSource();
		
		Connection cn = bd.getConnection();
		cn.setAutoCommit(false);
		Statement st = cn.createStatement();

//		int lignes = st.executeUpdate(
//			"update enseignant set nbhdisp = nbhdisp + 1.5 where idenseign = 'PGM'");
//
//		System.out.println(lignes + " lignes insérées");
//
//		lignes = st.executeUpdate(
//			"delete from affecter where heuredc = '11:00'");
//
//		lignes = st.executeUpdate(
//			"delete from enseigner where heuredc = '11:00'");
//
//		lignes = st.executeUpdate(
//			"delete from creneau where heuredc = '11:00'");
		
		int lignes = st.executeUpdate(
			"insert into enseignant (idenseign, nom, prenom, grade)"
			+ "values ('TST', 'moi', 'même', 'CDOC')");

		System.out.println(lignes + " lignes insérées");

		st.close();
//		cn.rollback();
		cn.close();
//		oracle.jdbc.datasource.impl.OracleDataSource.cleanup();

	}

}
