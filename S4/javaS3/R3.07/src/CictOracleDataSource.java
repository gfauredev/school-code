import java.sql.*;

import oracle.jdbc.pool.OracleDataSource;

public class CictOracleDataSource extends OracleDataSource {
	//private static final long serialVersionUID = 1L;

	public CictOracleDataSource () throws SQLException {
		this.setURL("jdbc:oracle:thin:@telline.univ-tlse3.fr:1521:etupre");
		this.setUser("frg4266a");
		this.setPassword("password");
	}
	
	public static void main(String[] args) throws SQLException {
		if((new CictOracleDataSource()).getConnection() != null) {
			System.out.println("Connexion OK");
		}
	}
}