package tp;

public class CompteBancaire2 {
	private int numero;
	private double solde;
	private Journal2Singleton journal;

	public CompteBancaire2(int numero) {
		this.numero = numero;
		this.solde = 0.0;
		this.journal = Journal2Singleton.get();
	}

	public void d�poser(double montant) {
		if (montant > 0.0) {
			solde += montant;
			this.journal.ajouterLog("D�p�t de " + montant + "� sur le compte " + numero + ".");
		} else {
			this.journal.ajouterLogCritique("D�p�t d'une valeur n�gative impossible (" + numero+ ").");
		}
	}
	
	public void retirer(double montant) {
		if (montant > 0.0) {
			if (solde >= montant) {
				solde -= montant;
				this.journal.ajouterLog("Retrait de " + montant + "� sur le compte " + numero+ ".");
			} else {
				this.journal.ajouterLogCritique("La banque n'autorise pas de d�couvert (" + numero+ ").");
			}
		} else {
			this.journal.ajouterLogCritique("Retrait d'une valeur n�gative impossible (" + numero+ ").");
		}
	}
	
	public Journal2Singleton getJournal() {
		return journal;
	}
}
