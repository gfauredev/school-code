package tp;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Journal2Singleton {
	private static Journal2Singleton instance;
	private StringBuffer log = new StringBuffer();
	private StringBuffer logCritique = new StringBuffer();

	private Journal2Singleton() {
	}

	public static synchronized Journal2Singleton get()
	{
		if (instance == null) instance = new Journal2Singleton();
		return instance;
	}

	public void ajouterLog(String log) {
		Date d = new Date();
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH'h'mm'm'ss's'SSS");
		this.log.append("[" + dateFormat.format(d) + "] " + log + "\n");
	}

	public void ajouterLogCritique(String log) {
		Date d = new Date();
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH'h'mm'm'ss's'SSS");
		this.logCritique.append("[" + dateFormat.format(d) + "] /!\\ " + log + "\n");
	}

	@Override
	public String toString() {
		return this.log.toString() + this.logCritique.toString();
	}
	
    public String toString(int niveau) {
    	if (niveau == 0) return this.log.toString();
    	if (niveau == 1) return this.logCritique.toString();
    	return this.toString();
    }
}
