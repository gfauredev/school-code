package tp;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class JournalSingleton {
	private static JournalSingleton instance;
	private StringBuffer log = new StringBuffer();

	private JournalSingleton() {
	}

	public static synchronized JournalSingleton get()
	{
		if (instance == null)
			instance = new JournalSingleton();

		return instance;
	}

	public void ajouterLog(String log) {
		Date d = new Date();
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH'h'mm'm'ss's'SSS");
		this.log.append("[" + dateFormat.format(d) + "] " + log + "\n");
	}

	@Override
	public String toString() {
		return this.log.toString();
	}
}
