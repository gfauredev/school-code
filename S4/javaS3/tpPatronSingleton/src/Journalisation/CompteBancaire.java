package Journalisation;

public class CompteBancaire {
	private int numero;
	private double solde;
	private Journal journal;

	public CompteBancaire(int numero) {
		this.numero = numero;
		this.solde = 0.0;
		this.journal = new Journal();
	}

	public void d�poser(double montant) {
		if (montant > 0.0) {
			solde += montant;
			this.journal.ajouterLog("D�p�t de " + montant + "� sur le compte " + numero + ".");
		} else {
			this.journal.ajouterLog("/!\\ D�p�t d'une valeur n�gative impossible (" + numero+ ").");
		}
	}
	
	public void retirer(double montant) {
		if (montant > 0.0) {
			if (solde >= montant) {
				solde -= montant;
				this.journal.ajouterLog("Retrait de " + montant + "� sur le compte " + numero+ ".");
			} else {
				this.journal.ajouterLog("/!\\ La banque n'autorise pas de d�couvert (" + numero+ ").");
			}
		} else {
			this.journal.ajouterLog("/!\\ Retrait d'une valeur n�gative impossible (" + numero+ ").");
		}
	}
	
	public Journal getJournal() {
		return journal;
	}
}
