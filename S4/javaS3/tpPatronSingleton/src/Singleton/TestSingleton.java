package Singleton;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestSingleton {

	@Test
	public void testNotNull() {
		assertNotNull(Singleton.getInstance());
	}
	
	@Test
	public void testTypeSingleton() {
		assertTrue(Singleton.getInstance() instanceof Singleton);
	}
	
	@Test
	public void testStaticObject() {
		assertEquals(Singleton.getInstance(), Singleton.getInstance());
	}

}
