from flask import Flask, request
from flask_restx import Api, Resource, fields
import kafka_producer

app = Flask(__name__)
api = Api(app, version='0.1', title='API Python', description='Add an entity')

namespace_entity = api.namespace('entite', description='Operations on entities')

entity_model = api.model('Entity', {
    'id': fields.integer(readonly=True, description='UID of entity', example=1),
    'entity_label': fields.String(required=True, description='Label of entity', example='My entity'),
    'entity_content': fields.String(required=True, description='Content of entity', example='My content')
})

@namespace_entity.route('/<int:id>')
class EntityIdQueries(Resource):
    @namespace_entity.doc('Add a new entity')
    @namespace_entity.expect('Add a new entity')
    @namespace_entity.response(201,'Success')
    @namespace_entity.response(400,'Fail')
    def post(self, id):
        # Add a new entity
        print(f"Receiving a new request of entity creation, id={id}")
        if 'entity_label' not in api.payload or api.payload['entity_label'] == '':
            return 'Label not found or empty', 400

        if 'entity_content' not in api.payload or api.payload['entity_content'] == '':
            return 'Content not found or empty', 400

        api.payload['id'] = id
        success = True
        print(f"Put the entity in the topic {id}")
        success = kafka_producer.put_in_topic(api.payload)
        if success:
            return 'Entity added', 201
        else:
            return 'Entity not added', 400
