from pymongo import MongoClient

db_host = 'database'
db_port = 27017

class DataAccessMongo(object):
    def __init__(self):
        self.client = MongoClient(db_host, db_port)
        self.db = self.client['DB']
        self.entity_collection = self.db['Entity']

    def add_entity(self, entity):
        self.entity_collection.insert_one(entity)
