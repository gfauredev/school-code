import json
import streamlit as st
import requests

st.title('Entity creator')

id = st.number_input('id', step=1)
entity_label = st.text_input('entity_label')
entity_content = st.text_input('entity_content')

if st.button('Add'):
    url = f"http://api:5000/entity/{id}"

    payload = json.dumps({
        'entity_label': entity_label,
        'entity_content': entity_content
    })
    headers = {
        'Content-Type': 'application/json'
    }

    response = requests.request("POST", url, headers=headers, data=payload)

    print(response.text)
