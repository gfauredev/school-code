<?php
const API = 'http://localhost/school/phpAPIs/rest/srv.php';

function post(string $data_string)
{
  return json_decode(file_get_contents(
    API,
    false,
    stream_context_create(array(
      'http' => array(
        'method' => 'POST',
        'content' => $data_string,
        'header' => array('Content-Type: application/json' . "\r\n"
          . 'Content-Length: ' . strlen($data_string) . "\r\n")
      )
    ))
  ), true);
}

function put(string $get_params, string $data_string)
{
  return json_decode(file_get_contents(
    API . $get_params,
    false,
    stream_context_create(array(
      'http' => array(
        'method' => 'PUT',
        'content' => $data_string,
        'header' => array('Content-Type: application/json' . "\r\n"
          . 'Content-Length: ' . strlen($data_string) . "\r\n")
      )
    ))
  ), true);
}

function get(string $get_params = '')
{
  return json_decode(file_get_contents(
    API . $get_params,
    false,
    stream_context_create(array('http' => array('method' => 'GET')))
  ), true);
}

function delete(string $get_params)
{
  return json_decode(file_get_contents(
    API . $get_params,
    false,
    stream_context_create(array('http' => array('method' => 'DELETE')))
  ), true);
}

$in = $_POST;
$sub = isset($in['submitter']) ? $in['submitter'] : 'NONE';
unset($in['submitter']);
if (!empty($in['id'])) {
  $id = '?id=' . $in['id'];
} else {
  $id = null;
  unset($in['id']);
}
// var_dump($in);

switch ($sub) {
  case 'POST':
    if (isset($id)) {
      // var_dump($id);
      $rep = put($id, json_encode($in));
    } else {
      $rep = post(json_encode($in));
    }
    break;
  case 'DELETE':
    if (isset($id)) {
      // var_dump($id);
      $rep = delete($id);
      break;
    }
  case 'GET':
    if (isset($id)) {
      // var_dump($id);
      $rep = get($id);
      break;
    }
  default:
    $rep = get();
    break;
}
// var_dump($rep);
?>
<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="style.css">
  <title>REST Client</title>
</head>

<body>
  <main>
    <h1>Client REST</h1>
    <section>
      <h2 class="technical">Actions</h2>
      <form action="." method="POST">
        <label>Identifiant (laisser vide pour tout voir)
          <input type="number" name="id" placeholder="1">
        </label>
        <label>Phrase
          <textarea rows="1" name="phrase" placeholder="Chuck Norris …"></textarea>
        </label>

        <button type="submit" name="submitter" value="POST">
          Créer / Modifier
        </button>
        <button type="submit" name="submitter" value="GET">
          Voir
        </button>
        <button type="submit" name="submitter" value="DELETE">
          Supprimer
        </button>
      </form>
    </section>
    <section>
      <h2>Réponse de l’API REST</h2>
      <details>
        <summary>Retour de <?= API ?></summary>
        <p><?= $rep['status'] . ' : ' . $rep['status_message'] ?></p>
      </details>
      <?php if (is_array($rep['data'])) {
        foreach ($rep['data'] as $i) { ?>
          <article id="<?= $i['id'] ?>">
            <h6>Phrase <?= $i['id'] ?></h6>
            <p><?= $i['phrase'] ?></p>
          </article>
      <?php }
      } else if (is_bool($rep['data'])) {
        echo $rep['data'] ? 'OK' : 'NO';
      }
      ?>
    </section>
  </main>
</body>

</html>
