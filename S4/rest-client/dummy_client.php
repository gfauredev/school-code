<?php
#!php
function pretty($res)
{
  var_dump($res);
  $res = json_decode($res, true);
  var_dump($res);
}

function get_data($res)
{
  return json_decode($res, true)['data'];
}

const RESSOURCE = 'http://localhost/school/phpAPIs/rest/srv.php';

echo '<h1>GET last 5</h1>';
$res = file_get_contents(
  RESSOURCE,
  false,
  stream_context_create(array('http' => array('method' => 'GET')))
);
var_dump($res);
$res = json_decode(json_decode($res, true)['data'], true);
var_dump(array_slice($res, -5, 10));

echo '<h1>POST new data</h1>';
$data = array('phrase' => 'Chuck ne connait pas le REST. Il préfère l’action.');
$data_string = json_encode($data);
// Envoi de la requête
$res = file_get_contents(
  RESSOURCE,
  false,
  stream_context_create(array(
    'http' => array(
      'method' => 'POST', // ou PUT
      'content' => $data_string,
      'header' => array('Content-Type: application/json' . "\r\n"
        . 'Content-Length: ' . strlen($data_string) . "\r\n")
    )
  ))
);
pretty($res);
$last_id = get_data($res)['id'];

echo '<h1>GET id=' . $last_id . '</h1>';
$res = file_get_contents(
  RESSOURCE . '?id=' . $last_id,
  false,
  stream_context_create(array('http' => array('method' => 'GET')))
);
pretty($res);

echo '<h1>GET last 5</h1>';
$res = file_get_contents(
  RESSOURCE,
  false,
  stream_context_create(array('http' => array('method' => 'GET')))
);
var_dump($res);
$res = json_decode(json_decode($res, true)['data'], true);
var_dump(array_slice($res, -5, 10));

echo '<h1>PUT id=' . $last_id . '</h1>';
$data = array('phrase' => 'Chuck ne connait pas le SOAP. Il est naturellement propre.');
$data_string = json_encode($data);
// Envoi de la requête
$res = file_get_contents(
  RESSOURCE . '?id=' . $last_id,
  false,
  stream_context_create(array(
    'http' => array(
      'method' => 'PUT', // ou PUT
      'content' => $data_string,
      'header' => array('Content-Type: application/json' . '\r\n'
        . 'Content-Length: ' . strlen($data_string) . '\r\n')
    )
  ))
);
pretty($res);

echo '<h1>GET id=' . $last_id . '</h1>';
$res = file_get_contents(
  RESSOURCE . '?id=' . $last_id,
  false,
  stream_context_create(array('http' => array('method' => 'GET')))
);
pretty($res);

echo '<h1>DELETE id=' . $last_id . '</h1>';
$res = file_get_contents(
  RESSOURCE . '?id=' . $last_id,
  false,
  stream_context_create(array('http' => array('method' => 'DELETE')))
);
var_dump($res);

echo '<h1>GET last 30</h1>';
$res = file_get_contents(
  RESSOURCE,
  false,
  stream_context_create(array('http' => array('method' => 'GET')))
);
var_dump($res);
$res = json_decode(json_decode($res, true)['data'], true);
var_dump(array_slice($res, -30, 50));
